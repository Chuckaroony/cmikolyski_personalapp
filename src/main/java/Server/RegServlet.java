package Server;

import Control.RespControl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "RegServlet", urlPatterns = {"/RegServlet"})
public class RegServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = null;
        try {
            response.setContentType("text/html");
            out = response.getWriter();
            String fName = request.getParameter("fName").toLowerCase();
            String lName = request.getParameter("lName").toLowerCase();
            String phone = request.getParameter("phone");
            String email = request.getParameter("email").toLowerCase();
            out.println(RespControl.handleRequest(fName, lName, phone, email));
        } catch ( ExceptionInInitializerError e) {
            out.println("It seems the database is down. Please try again in a short time.");
        } catch (Exception e) {
            out.println("Something is wrong. We are working on fixing the issue. Please try again in a short time.");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
