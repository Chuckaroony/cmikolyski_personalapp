package Hibernate;

import javax.persistence.*;

@Entity
@Table(name = "attendees")
public class Attendee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int idplayer;

    @Column(name = "firstname", unique = true, nullable = false )
    private String firstname;

    @Column(name = "lastname", unique = true, nullable = false )
    private String lastname;

    @Column(name = "phone", nullable = false)
    private  String phone;

    @Column(name = "email", nullable = false)
    private  String email;

    public Attendee() {
    }

    public Attendee(String fname, String lname, String phone, String email) {
        this.firstname = fname;
        this.lastname = lname;
        this.phone = phone;
        this.email = email;
    }



    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdplayer() {
        return idplayer;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}

