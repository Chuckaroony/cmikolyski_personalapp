package Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;

import java.util.List;

public class DataAccessObjects {

    SessionFactory factory = null;
    Session session = null;

    private static DataAccessObjects single_instance = null;

    private DataAccessObjects()
    {
        factory = HibernateUtilities.getSessionFactory();
    }

    public static DataAccessObjects getInstance() {
        if (single_instance == null) {
            single_instance = new DataAccessObjects();
        }

        return single_instance;
    }


    public List<Attendee> getAttendees() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Attendee";
            List<Attendee> queryList = (List<Attendee>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return queryList;

        } catch (Exception e) {

            System.out.println("Unable to perform request. Unable to contact " +
                    "database or something is wrong with the query.");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Attendee getAttendee(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Attendee where id=" + Integer.toString(id);
            Attendee theAttendee = (Attendee) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return theAttendee;

        } catch (Exception e) {

            System.out.println("There is a problem with the query.");

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public void insertAttendee(Attendee newAttendee) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(newAttendee);
            session.getTransaction().commit();
        } catch (ConstraintViolationException e) {

            System.out.println("There is a duplicate record");
            session.getTransaction().rollback();
        } catch (Exception e) {

            System.out.println("There is a problem registering.");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

}
