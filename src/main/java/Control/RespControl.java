package Control;

import Hibernate.Attendee;
import Hibernate.DataAccessObjects;

public class RespControl {
    public static String handleRequest( String fname, String lname, String phone, String email) {

        String msgallReadyReg = "That information is already registered for the event.";
        String msgComplete = "Registration complete!";
        String msgIncomplete = "The information is in an incorrect format. Please follow the form instructions.";
        String regComplete = "<head><title>Registration Response</title></head> <body> <p>" + msgComplete + "<p> </body>";
        String allReadyReg = "<head><title>Registration Response</title></head> <body> <p>" + msgallReadyReg + "<p> </body>";
        String regIncomplete = "<head><title>Registration Response</title></head> <body> <p>" + msgIncomplete + "<p> </body>";

        if (MyValidation.ValidateInput(fname, lname, phone, email)) {
            if (!MyValidation.checkMatch(fname, lname, email)) {
                Attendee newAttendee = new Attendee(fname, lname, phone, email);
                DataAccessObjects dbInstance = DataAccessObjects.getInstance();
                dbInstance.insertAttendee(newAttendee);

                return regComplete;
            } else {

                return allReadyReg;
            }
        } else {

            return regIncomplete;
        }
    }
}
