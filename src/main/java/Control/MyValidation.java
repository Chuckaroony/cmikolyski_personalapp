package Control;

import Hibernate.Attendee;
import Hibernate.DataAccessObjects;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyValidation {

    public static boolean ValidateInput(String fname, String lname, String phone, String email) {
        int maxLength = 45;
        int phoneLength = 10;
        Pattern namePattern = Pattern.compile("[^a-z]", Pattern.CASE_INSENSITIVE);
        Pattern phonePattern = Pattern.compile("[^0-9]");
        Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9_#$%&’*+/=?^.-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$");


        if ((fname != null && fname.length() <= maxLength && !fname.equals("")) && (lname != null && lname.length() <= maxLength && !lname.equals("")) &&
                (phone != null && phone.length() == phoneLength && !phone.equals("")) && (email != null && email.length() <= maxLength && !email.equals(""))) {
            Matcher fMatcher = namePattern.matcher(fname);
            Matcher lMatcher = namePattern.matcher(lname);
            Matcher phoneMatcher = phonePattern.matcher(phone);
            Matcher emailMatcher = emailPattern.matcher(email);
            boolean fMatch = fMatcher.find();
            boolean lMatch = lMatcher.find();
            boolean phoneMatch = phoneMatcher.find();
            boolean emailMatch = emailMatcher.find();

            if (!emailMatch || fMatch || lMatch || phoneMatch) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }


    public static boolean checkMatch(String fname, String lname, String email) {
        DataAccessObjects dbInstance = DataAccessObjects.getInstance();

        List<Attendee> attendees = dbInstance.getAttendees();
        for (Attendee i : attendees) {
            if ((i.getFirstname().equalsIgnoreCase(fname)) && (i.getLastname().equalsIgnoreCase(lname)) && (i.getEmail().equalsIgnoreCase(email)) ){
                return true;
            }
        }
        return false;
    }

}
