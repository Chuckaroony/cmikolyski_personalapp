<%--
  Created by IntelliJ IDEA.
  User: Chuck
  Date: 11/30/2020
  Time: 7:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>


<form action="RegServlet" method="post">
    <h1>Welcome to the event registration system!</h1>
    <p>Please fill in the form below with the required information.<br>
    This information will be used to check that you are registered the day of the event.</p>
    <p>First Name <input name="fName" type="text" /> </p>
    <p>Last Name <input name="lName" type="text" /> </p>
    <p>Please include only the area code followed by the phone number. Do not include parenthesis, dashes, spaces, etc.</p>
    <p>Phone <input name="phone" type="text" /> </p>
    <p>Email <input name="email" type="text" /> </p>
    <p>Submit <input type="submit" value="submit" /></p>
</form>

</body>
</html>
